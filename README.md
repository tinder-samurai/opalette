# [![Get it on Google Play](https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png =150x)](https://play.google.com/store/apps/details?id=net.henryco.opalette&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1)
#### A simple application that allows you to create a color palette based on the image, supports color and convolutional filters, as well as various kinds of image transformation, such as pixelization, dithering, blurring and all possible color settings.
  

**Update 6 (12.04.17): **

**Well, project finaly rolled to the release!**
![screen](https://raw.githubusercontent.com/henryco/OPalette/master/promo/release/1.png)  
  
  
![screen](https://raw.githubusercontent.com/henryco/OPalette/master/promo/release/example_dark.png)  
  
  
![screen](https://raw.githubusercontent.com/henryco/OPalette/master/promo/release/2.png)  
  
  
![screen](https://raw.githubusercontent.com/henryco/OPalette/master/promo/release/3.png)  
  
  
![screen](https://raw.githubusercontent.com/henryco/OPalette/master/promo/release/4.png)  
  
  
![screen](https://raw.githubusercontent.com/henryco/OPalette/master/promo/release/5.png)  
  
  
![screen](https://raw.githubusercontent.com/henryco/OPalette/master/promo/release/6.png)  
  
  
![screen](https://raw.githubusercontent.com/henryco/OPalette/master/promo/release/7.png)  
  
  
![screen](https://raw.githubusercontent.com/henryco/OPalette/master/promo/release/8.png)  
  
  
![screen](https://raw.githubusercontent.com/henryco/OPalette/master/promo/release/9.png)  
  
  
![screen](https://raw.githubusercontent.com/henryco/OPalette/master/promo/release/10.png)  
  


**Update 5 (11.03.17):**
First project visual identity
![screen](https://raw.githubusercontent.com/henryco/OPalette/master/logo/identity.png)
  
  
**Update 4 (9.03.17):**
![screen](https://raw.githubusercontent.com/henryco/OPalette/master/promo/opall1a.png)
  
  
**Update 3:**
![screen](https://raw.githubusercontent.com/henryco/OPalette/master/promo/picker.png)
  
  
**Update 2:**
![screen](https://raw.githubusercontent.com/henryco/OPalette/master/promo/secondLook.png)
  
  
**Update 1:**
![screen](https://raw.githubusercontent.com/henryco/OPalette/master/promo/firstLook.png)
  
  
